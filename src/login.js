import React, { useState } from 'react';
import { StyleSheet, Text, View, TextInput, TouchableOpacity } from 'react-native';
import { AntDesign, Feather } from '@expo/vector-icons'; 
import { LinearGradient } from 'expo-linear-gradient';
import { useFonts } from 'expo-font'; 
import rotas from './api'; 

const Login = ({ navigation }) => {
  // Estados para gerenciar o formulário
  const [usuario, setUsername] = useState('');
  const [senha, setPassword] = useState('');
  const [showPassword, setShowPassword] = useState(false);
  const [isPasswordSet, setIsPasswordSet] = useState(false);
  const [loginStatus, setLoginStatus] = useState(null);

  // Carregar a fonte
  const [loaded] = useFonts({
    MinhaFonte: require('../assets/fonts/LeagueGothic-Regular.ttf'),
  });

  if (!loaded) {
    return null; // ou algum tipo de indicador de carregamento
  }

  // Função de login assíncrona para autenticação via API

const login = async () => {
  try {
    const response = await rotas.logUser({ usuario, senha });

    if (response.status === 200) {
      setLoginStatus("success");  // Define o estado de login como sucesso
      navigation.navigate('Pagina1');
    } else {
      setLoginStatus("failure");  // Define o estado de login como falha
    }
  } catch (error) {
    console.error("Erro ao fazer login:", error);
    setLoginStatus("failure");  // Define o estado de login como falha em caso de erro
  }
};



  return (
    <View style={styles.container}>
      <View style={styles.titleContainer}>
        <Text style={styles.title}>Login</Text>
      </View>

      <View style={styles.usuarioContainer}>
        <Text style={styles.senhaText}>
          Usuário
        </Text>
      </View>
      <View style={styles.inputContainer}>
        <AntDesign name="user" size={24} color={'black'} style={styles.icon} />
        <TextInput
          style={styles.input}
          placeholder="Digite seu usuário"
          value={usuario}
          onChangeText={setUsername}
        />
      </View>

      <View style={styles.senhaContainer}>
        <Text style={styles.senhaText}>
          Senha
        </Text>
      </View>
      <View style={styles.inputContainer}>
        <AntDesign name="lock" size={24} color={'black'} style={styles.icon} />
        <TextInput
          style={styles.input}
          placeholder="Digite sua senha"
          secureTextEntry={!showPassword}
          value={senha}
          onChangeText={(text) => {
            setPassword(text);
            setIsPasswordSet(text.length > 0); // Define como true se o comprimento do texto for maior que 0
          }}
        />
        {isPasswordSet && ( // Renderiza o ícone do olho apenas quando a senha não está vazia
          <Feather
            name={showPassword ? "eye-off" : "eye"} 
            size={24} 
            color="black" 
            onPress={() => setShowPassword(!showPassword)} 
            style={styles.eyeIcon}
          />
        )}
      </View>

      <View style={styles.autenticado}>
        {loginStatus === "success" && <Text style={[styles.statusText, { color: 'green' }]}>Login efetuado com sucesso!</Text>}
        {loginStatus === "failure" && <Text style={[styles.statusText, { color: 'red' }]}>Usuário ou senha incorretos.</Text>}
        <Text style={styles.link}>Esqueceu sua senha?</Text>
      </View>

      <View style={styles.botaoContainer}>
        <TouchableOpacity style={styles.botao} onPress={usuario && senha ? login : null}>
          <LinearGradient
            colors={["#203AA4", "#5B6FC1", "#8696D8"]}
            style={styles.linearGradient}

            start={{ x: 0, y: 2 }}
            end={{ x: 1, y: 0 }}
          >
            <Text style={styles.botaoText}>Login</Text>
          </LinearGradient>
        </TouchableOpacity>
      </View>
      <View>
        <TouchableOpacity onPress={() => navigation.navigate('Cadastro')}>
          <Text style={styles.register}>Ainda não possui uma conta?</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#fff'
  },
  titleContainer: {
    marginTop: 150,
  },
  title: {
    fontSize: 60,
    fontFamily:'MinhaFonte',
    color: '#203AA4',
  },
  inputContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 50,
    borderBottomWidth: 1,
    borderColor: 'black', // Alteração da cor da borda
    borderRadius: 5,
    paddingHorizontal: 10,
  },
  input: {
    flex: 0.8,
    height: 40,
    color: 'black', // Cor do texto
  },
  icon: {
    marginRight: 5,
  },
  link: {
    fontSize: 12,
    color: '#000',
    left:90,
  },
  register: {
    fontSize: 15,
    color: '#203AA4',
    marginTop: 10,
  },
  botaoContainer: {
    marginTop: 50,
  },
  botao: {
    alignItems: 'center',
    justifyContent: 'center',
    height: 50,
    width: 150,
    borderRadius: 25,
    overflow: 'hidden',
    marginBottom: 10,
    marginTop: 20,
    paddingHorizontal: 12,
  },
  botaoText: {
    color: '#fff',
    fontSize: 16,
    fontWeight: 'bold',
  },
  linearGradient: {
    alignItems: 'center',
    justifyContent: 'center',
    height: 50,
    width: 150,
    borderRadius: 25,
  },
  usuarioContainer:{
    top:50,
    right:112,
  },
  senhaContainer:{
    top:50,
    right:120
  },
  statusText:{
    fontSize:12,
    position:'absolute',
    right:50
  },

  
});

export default Login;
