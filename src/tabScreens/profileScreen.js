import React from 'react';
import { View, Text, StyleSheet,TouchableOpacity } from 'react-native';
import { FontAwesome, MaterialIcons } from '@expo/vector-icons';


const SimplePage = ({ navigation }) => {
  const logout = () => {
    navigation.navigate('Home');
  }
  return (
    <View style={styles.container}>
        <View>
            <View style={styles.Bt1}>
            <TouchableOpacity style={styles.Bt1Container}>
                <Text style={styles.titleBt1}>Perfil</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.logoutContainer} onPress={logout}>
                <Text style={styles.textLogout}>Logout</Text>
            </TouchableOpacity>

            </View>
        </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  Bt1Container:{
    flexDirection:'row'
  },
  titleBt1:{
    fontSize:20
  },
  text: {
    fontSize: 24,
    fontWeight: 'bold',
  },
  logoutContainer:{
    backgroundColor:'blue',
    height:40,
    width:60,
    justifyContent:'center',
    alignItems:'center',
    borderRadius:15,
  },
  textLogout:{
    color:'white'

  }
});

export default SimplePage;
