import axios from "axios"

const api = axios.create({
    baseURL: "http://localhost:5000/hotel",
    header: {
        'accept': 'application/json', },
});

const rotas = {

    logUser:(user) => api.post("/login", user),
    postUser:(user)=> api.post("/cadastroUser", user),
    putUser:(user)=> api.put("/atualizarUser", user),
    deleteUser:(_id)=> api.delete("/deleteUser/:id"+_id),
    signInUser:(user)=> api.post("/signin", user)


}

export default rotas;
