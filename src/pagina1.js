import React from 'react';
import { View, StyleSheet, Platform, StatusBar } from 'react-native'; // Importando Platform e StatusBar
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { FontAwesome, MaterialIcons } from '@expo/vector-icons';

import HomeScreen from './tabScreens/homeScreen';
import SearchScreen from './tabScreens/searchScreen';
import ProfileScreen from './tabScreens/profileScreen';
import Header from './header';

const Tab = createBottomTabNavigator();

const BottomTabNavigator = () => {
  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Header />
      </View>

      <View style={styles.tabNavigator}>
        <Tab.Navigator
          screenOptions={({ route }) => ({
            headerShown: false,
            tabBarIcon: ({ color, size }) => {
              let iconName;

              if (route.name === 'HomeScreen') {
                iconName = 'home';
                return <FontAwesome name={iconName} size={size} color={color} />;
              } else if (route.name === 'Search') {
                iconName = 'search';
                return <FontAwesome name={iconName} size={size} color={color} />;
              } else if (route.name === 'Profile') {
                iconName = 'account-circle';
                return <MaterialIcons name={iconName} size={size} color={color} />;
              }
            },
            tabBarActiveTintColor: 'black',
            tabBarInactiveTintColor: 'gray',
          })}
        >
          <Tab.Screen name="HomeScreen" component={HomeScreen} />
          <Tab.Screen name="Search" component={SearchScreen} />
          <Tab.Screen name="Profile" component={ProfileScreen} />
        </Tab.Navigator>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    paddingTop: Platform.OS === 'android' ? StatusBar.currentHeight : 0,
  },
  tabNavigator: {
    flex: 1,
  },
});

export default BottomTabNavigator;
