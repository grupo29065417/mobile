import { CurrentRenderContext } from '@react-navigation/native';
import React from 'react';
import { View, SafeAreaView, StyleSheet, Platform, StatusBar, Image, Text, TouchableOpacity } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import { useFonts } from 'expo-font'; 



const Home = ({ navigation }) => {
    const [loaded] = useFonts({
        MinhaFonte: require('../assets/fonts/LeagueGothic-Regular.ttf'),
      });

      if (!loaded) {
        return null; // ou algum tipo de indicador de carregamento
      }
    
    return (
        <View style={styles.container}>
            <View style={styles.content}>
                <View style={styles.imagemContainer}>
                    <Image
                        source={require('../assets/planoDeFundo2.jpg')}
                        style={{ width: '100%', height: 350, position: 'absolute', opacity: 0.6}}
                    />
                    <Text style={styles.title}>QuartoCerto</Text>
                    <Text style={styles.subTitle}>Bem vindo ao aplicativo de reserva de hoteis </Text>
                </View>
                <View style={styles.logins}>
                    <Text style={styles.textProsseguir}>Reserva de hoteis e pousadas para todo Brasil</Text>
                    <TouchableOpacity style={styles.botao} onPress={() => { navigation.navigate('Login') }}>
                        <LinearGradient
                            colors={["#203AA4", "#5B6FC1", "#8696D8"]}
                            style={styles.linearGradient}
                            start={{ x: 0, y: 2 }}
                            end={{ x: 1, y: 0 }}
                        >
                            <Text style={styles.botaoText}>Continuar</Text>
                        </LinearGradient>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    content: {
        flex: 1,
        paddingTop: Platform.OS === 'android' ? StatusBar.currentHeight : 0,
        // Adicione outras estilizações conforme necessário
    },
    imagemContainer: {
        alignItems: 'center',
        flex: 0.48,

    },
    title: {
        fontSize: 60,
        color: "black",
        fontFamily:'MinhaFonte',
        top: 130

    },
    subTitle: {
        color: 'black',
        fontSize: 25,
        fontFamily:'MinhaFonte',
        top: 160
    },
    logins: {
        alignItems: 'center',
        justifyContent: 'space-evenly',
        flex: 0.52,
    },
    textProsseguir:{
        fontSize:22,
        fontFamily:'MinhaFonte',
        color:"#203AA4"
    },
    botao: {
        alignItems: 'center',
        justifyContent: 'center',
        height: 60,
        width: 250,
        borderRadius: 25,
        overflow: 'hidden',
        marginBottom: 10,
        marginTop: 20,
        paddingHorizontal: 12,
    },
    botaoText: {
        color: '#fff',
        fontSize: 25,
        fontWeight: 'bold',
    },
    linearGradient: {
        alignItems: 'center',
        justifyContent: 'center',
        height: 60,
        width: 250,
        borderRadius: 25,
    },
});

export default Home;
