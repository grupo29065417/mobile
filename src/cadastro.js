import React, { useState } from 'react';
import { StyleSheet, Text, View, TextInput, TouchableOpacity } from 'react-native';
import { AntDesign, Feather, Fontisto  } from '@expo/vector-icons'; // Importando ícones do pacote expo
import { LinearGradient } from 'expo-linear-gradient';
import { useFonts } from 'expo-font'; 
import rotas from './api';

const Cadastro = ({ navigation }) => {
  const [usuario, setUsername] = useState('');
  const [email, setEmail] = useState('');
  const [senha, setPassword] = useState('');
  const [confirmSenha, setConfirmPassword] = useState('');
  const [showPassword, setShowPassword] = useState(false); // Estado para controlar a visibilidade da senha
  const [showConfirmPassword, setShowConfirmPassword] = useState(false); // Estado para controlar a visibilidade da confirmação de senha
  const [isPasswordSet, setIsPasswordSet] = useState(false); // Estado para controlar se a senha foi definida
  const [isConfirmPasswordSet, setIsConfirmPasswordSet] = useState(false); // Estado para controlar se a senha foi definida
  const [CadastroStatus, setCadastroStatus] = useState(null); // Estado para controlar o status do login


  // Carregar a fonte
  const [loaded] = useFonts({
    MinhaFonte: require('../assets/fonts/LeagueGothic-Regular.ttf'),
  });

  if (!loaded) {
    return null; // ou algum tipo de indicador de carregamento
  }

  const Cadastro = async () => {
    try {
      const response = await rotas.signInUser({ usuario, senha, confirmSenha, email });
      if (response.status === 200) {
        setCadastroStatus("success");  
      } else {
        setCadastroStatus("failure");  
      }
    } catch (error) {
      console.error("Erro ao fazer cadastro:", error);
      setCadastroStatus("failure");  
    }
  }
  

  return (
    <View style={styles.container}>
      <View style={styles.titleContainer}>
        <Text style={styles.title}>Cadastro</Text>
      </View>
      <View style={styles.usuarioContainer}>
        <Text style={styles.senhaText}>
          Usuário
        </Text>
      </View>
      <View style={styles.inputContainer}>
        <AntDesign name="user" size={24} color={'black'} style={styles.icon} />
        <TextInput
          style={styles.input}
          placeholder="Digite seu usuário"
          value={usuario}
          onChangeText={setUsername}
        />
      </View>
      <View style={styles.emailContainer}>
        <Text style={styles.senhaText}>
          E-mail
        </Text>
      </View>
      <View style={styles.inputContainer}>
      <Fontisto name="email" size={24} color="black" style={styles.icon} />
        <TextInput
          style={styles.input}
          placeholder="Digite seu usuário"
          value={email}
          onChangeText={setEmail}
        />
      </View>
      <View style={styles.senhaContainer}>
        <Text style={styles.senhaText}>
          Senha
        </Text>
      </View>
      <View style={styles.inputContainer}>
  <AntDesign name="lock" size={24} color={'black'} style={styles.icon} />
  <TextInput
    style={styles.input}
    placeholder="Digite sua senha"
    secureTextEntry={!showPassword}
    value={senha}
    onChangeText={(text) => {
      setPassword(text);
      setIsPasswordSet(text.length > 0); // Define como true se o comprimento do texto for maior que 0
    }}
  />
  {isPasswordSet && ( // Renderiza o ícone do olho apenas quando a senha não está vazia
    <Feather 
      name={showPassword ? "eye-off" : "eye"} 
      size={24} 
      color="black" 
      onPress={() => setShowPassword(!showPassword)} 
      style={styles.eyeIcon}
    />
  )}
</View>
<View style={styles.confirmPassword}>
  <Text style={styles.senhaText}>
    Confirmar Senha
  </Text>
</View>
<View style={styles.inputContainer}>
  <AntDesign name="lock" size={24} color={'black'} style={styles.icon} />
  <TextInput
    style={styles.input}
    placeholder="Digite sua senha"
    secureTextEntry={!showConfirmPassword}
    value={confirmSenha}
    onChangeText={(text) => {
      setConfirmPassword(text);
      setIsConfirmPasswordSet(text.length > 0); 
    }}
  />
  {isConfirmPasswordSet && ( // Renderiza o ícone do olho apenas quando a senha não está vazia
    <Feather 
      name={showConfirmPassword ? "eye-off" : "eye"} 
      size={24} 
      color="black" 
      onPress={() => setShowConfirmPassword(!showConfirmPassword)} 
      style={styles.eyeIcon}
    />
  )}
</View>

      <View style={styles.autenticado}>
      {CadastroStatus === "success" && <Text style={[styles.statusText, { color: 'green' }]}>Usuário cadastrado com sucesso.</Text>}
      {CadastroStatus === "failure" && <Text style={[styles.statusText, { color: 'red' }]}>Erro no cadastro.</Text>}
     
      </View>
     
      <View style={styles.botaoContainer}>
        <TouchableOpacity style={styles.botao} onPress={Cadastro}>
          <LinearGradient
            colors={["#203AA4", "#5B6FC1", "#8696D8"]}
            style={styles.linearGradient}
            start={{ x: 0, y: 2 }}
            end={{ x: 1, y: 0 }}
          >
            <Text style={styles.botaoText}>Cadastrar</Text>
          </LinearGradient>
        </TouchableOpacity>
      </View>
      <View>
        <TouchableOpacity onPress={() => navigation.navigate('Login')}>
          <Text style={styles.register}>Já possui uma conta?</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#fff'
  },
  titleContainer: {
    marginTop: 150,
  },
  title: {
    fontSize: 60,
    fontFamily:'MinhaFonte',
    color: '#203AA4',
  },
  inputContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderColor: 'black', // Alteração da cor da borda
    borderRadius: 5,
    paddingHorizontal: 10,
    marginTop:10
  },
  input: {
    flex: 0.8,
    height: 40,
    color: 'black', // Cor do texto
  },
  icon: {
    marginRight: 5,
  },
  link: {
    fontSize: 12,
    color: '#000',
    left:90,
  },
  register: {
    fontSize: 15,
    color: '#203AA4',
    marginTop: 10,
  },
  botaoContainer: {
    marginTop: 20,
  },
  botao: {
    alignItems: 'center',
    justifyContent: 'center',
    height: 50,
    width: 150,
    borderRadius: 25,
    overflow: 'hidden',
    marginBottom: 10,
    marginTop: 20,
    paddingHorizontal: 12,
  },
  botaoText: {
    color: '#fff',
    fontSize: 16,
    fontWeight: 'bold',
  },
  linearGradient: {
    alignItems: 'center',
    justifyContent: 'center',
    height: 50,
    width: 150,
    borderRadius: 25,
  },
  usuarioContainer:{
    top:10,
    right:112,
  },
  emailContainer:{
    top:10,
    right:118,
  },
  senhaContainer:{
    top:15,
    right:122
  },
  confirmPassword:{
    top:12,
    right:90
  },
  statusText:{
    fontSize:12,

  },
});

export default Cadastro;
