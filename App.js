import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Login from "./src/login";
import Home from "./src/home";
import Cadastro from './src/cadastro';
import Pagina1 from './src/pagina1';
const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home" screenOptions={{ headerShown: false }}>
        <Stack.Screen name='Login' component={Login}/>
        <Stack.Screen name='Cadastro' component={Cadastro}/>
        <Stack.Screen name='Home' component={Home}/>
        <Stack.Screen name='Pagina1' component={Pagina1}/>
      </Stack.Navigator>
    </NavigationContainer>
  );
}
